<?php

namespace LukasovsArtjoms\Test\Model;

class Test extends \Magento\Framework\Model\AbstractModel
{
    public function _construct()
    {
        $this->_init('LukasovsArtjoms\Test\Model\ResourceModel\Test');
    }
}
