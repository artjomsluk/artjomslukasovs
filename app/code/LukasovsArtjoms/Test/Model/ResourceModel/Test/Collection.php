<?php

namespace LukasovsArtjoms\Test\Model\ResourceModel\Test;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'id_badge';
    public function _construct()
    {
        $this->_init('LukasovsArtjoms\Test\Model\Test', 'LukasovsArtjoms\Test\Model\ResourceModel\Test');
    }
}